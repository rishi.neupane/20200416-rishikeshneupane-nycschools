This project is an assignment to get List of NYC Schools and score details from
given api. Due to limited time, I did not focus on UI.
Language: Java and Kotlin
Design Architecture: MVVM
Network call: Retrofit

Below are the main features;
1. Show the list of NYC schools
2. Navigate to show school details with SAT score when click to see detail
3. Navigate to google maps
4. Use device to call the school