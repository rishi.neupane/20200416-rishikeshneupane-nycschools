package android.jpmchase.rishikeshneupane_nycschools;

import android.jpmchase.rishikeshneupane_nycschools.data.model.School;
import android.jpmchase.rishikeshneupane_nycschools.utilties.CollectionUtiliy;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Unit test, which will execute Unit testable Utility functionality.

 */
public class UtilityTest {
    @Before

    @Test
    public void filterSchoolTests() {
        List<School> schools = new ArrayList<>();
        School s1 = new School();
        s1.setSchoolName("Chirag, Galyang");
        School s2 = new School();
        s2.setSchoolName("Adarsha, Galyang");
        School s3 = new School();
        s3.setSchoolName("Pioner's, Walling");
        School s4 = new School();
        s4.setSchoolName("Adarsha, Waling");
        schools.add(s1);
        schools.add(s2);
        schools.add(s3);
        schools.add(s4);
        assertEquals(CollectionUtiliy.filterSchools(schools, "Ri").size(), 0);
        assertEquals(CollectionUtiliy.filterSchools(schools, "Adarsha").size(), 2);
        assertEquals(CollectionUtiliy.filterSchools(schools, "Chirag").size(), 1);
        assertTrue(CollectionUtiliy.filterSchools(schools, "Chirag").contains(s1));
        assertTrue(CollectionUtiliy.filterSchools(schools, "Adarsha").contains(s2));
        assertTrue(CollectionUtiliy.filterSchools(schools, "Adarsha").contains(s4));
        assertFalse(CollectionUtiliy.filterSchools(schools, "Adarsha").contains(s1));
        assertFalse(CollectionUtiliy.filterSchools(schools, "Adarsha").contains(s3));
    }

}