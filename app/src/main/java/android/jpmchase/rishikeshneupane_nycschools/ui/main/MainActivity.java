package android.jpmchase.rishikeshneupane_nycschools.ui.main;

import android.content.Context;
import android.jpmchase.rishikeshneupane_nycschools.R;
import android.jpmchase.rishikeshneupane_nycschools.ui.fragments.SchoolListFragment;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;


import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    public Toolbar toolbar;
    private FragmentManager fm;
    SchoolListFragment schoolListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fm = getSupportFragmentManager();
        if (savedInstanceState == null) {
            schoolListFragment = new SchoolListFragment();
            fm.beginTransaction().add(R.id.fragment_container, schoolListFragment)
                    .commit();
        }
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.nyc_shools);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    public void setToolbarTitle(String toolbarTitle) {
        toolbar.setTitle(toolbarTitle);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public boolean isNetworkAvailable() {
        NetworkInfo activeNetworkInfo = null;
        try {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        } catch (Exception e) {

        }

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

}