package android.jpmchase.rishikeshneupane_nycschools.ui.fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.jpmchase.rishikeshneupane_nycschools.data.model.SchoolScore;
import android.jpmchase.rishikeshneupane_nycschools.data.model.School;
import android.jpmchase.rishikeshneupane_nycschools.data.net.NYCSchoolsNetworkCommunicator;
import android.jpmchase.rishikeshneupane_nycschools.ui.main.MainActivity;
import android.jpmchase.rishikeshneupane_nycschools.R;
import android.jpmchase.rishikeshneupane_nycschools.viewModel.SchoolViewModel;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static androidx.constraintlayout.widget.Constraints.TAG;

/**
 * This fragment hold school details
 */
public class SchoolResultsFragment extends Fragment {

    private TextView schoolOverview;
    private TextView schoolName;
    private TextView address1;
    private TextView address2;
    private TextView satTakers;
    private TextView scoreCriticalReading;
    private TextView scoreMath;
    private TextView scoreWriting;
    private ImageView directions;
    private ImageView phone;
    private ConstraintLayout resultCardHolder;
    private SchoolViewModel schoolViewModel;
    private School currentSchool;
    private TextView noDataMsg;
    private TextView networkErrorTxt;
    private Button retryBtn;
    private ConstraintLayout networkErrorView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_school_score, container, false);
        schoolViewModel = new ViewModelProvider(getActivity()).get(SchoolViewModel.class);

        //Checking instance to be safe in case this fragment is called from other activity in future
        if(getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).setToolbarTitle(getString(R.string.school_detail_text));
            ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        noDataMsg = root.findViewById(R.id.loading_data_msg);
        retryBtn = root.findViewById(R.id.retry_btn);
        networkErrorTxt = root.findViewById(R.id.error_message_txt);
        networkErrorView = root.findViewById(R.id.network_error_view);

        schoolOverview = root.findViewById(R.id.school_overview);
        schoolName = root.findViewById(R.id.school_name);
        satTakers = root.findViewById(R.id.no_of_sat_takers);
        scoreCriticalReading = root.findViewById(R.id.score_critical);
        scoreMath = root.findViewById(R.id.score_math);
        scoreWriting = root.findViewById(R.id.score_writing);
        resultCardHolder = root.findViewById(R.id.result_holder_cv);
        address1 = root.findViewById(R.id.school_address_line1);
        address2 = root.findViewById(R.id.town_and_zip_and_state);
        directions = root.findViewById(R.id.directions);
        phone = root.findViewById(R.id.phone);
        directions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             navigateToGoogleMaps();
            }
        });
        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callToSchool();
            }
        });

        schoolViewModel.getSchoolScoreMap().observe(getViewLifecycleOwner(), new Observer<Map<String, SchoolScore>>() {

            @Override
            public void onChanged(Map<String, SchoolScore> schoolScore) {
                populateSchoolDetailUI(schoolScore);
            }
        });
        if (schoolViewModel.getSchoolScoreMap().getValue() != null) {
            populateSchoolDetailUI(schoolViewModel.getSchoolScoreMap().getValue());
        } else {
            populateSchoolDetailUI(null);
            //Checking network connection before calling api
            if (getActivity() instanceof MainActivity && ((MainActivity) getActivity()).isNetworkAvailable()) {
                setLoadingMessageVisibility();
                networkErrorView.setVisibility(View.GONE);
                getScore();
            } else {
                setVisibilityForNoDataView();
                networkErrorTxt.setText(getContext().getResources().getString(R.string.no_network_message));
                //Toast.makeText(getActivity(), R.string.no_network_message, Toast.LENGTH_SHORT).show();
            }
        }

        retryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() instanceof MainActivity && ((MainActivity) getActivity()).isNetworkAvailable()) {
                    setLoadingMessageVisibility();
                    networkErrorView.setVisibility(View.GONE);
                    getScore();
                }

            }
        });

        return root;
    }

    private void setLoadingMessageVisibility() {
        if (schoolViewModel.getSchoolScoreMap().getValue() != null) {
            noDataMsg.setVisibility(View.GONE);
        } else {
            noDataMsg.setVisibility(View.VISIBLE);
        }
    }

    private void setVisibilityForNoDataView() {
        noDataMsg.setVisibility(View.GONE);
        networkErrorView.setVisibility(View.VISIBLE);
    }

    private void populateSchoolDetailUI(Map<String, SchoolScore> schoolScore) {
        Resources res = getResources();
        String schoolId = schoolViewModel.getSelectedSchoolId();
        for (School s : schoolViewModel.getSchoolList().getValue()) {
            if (s.getDbn().equalsIgnoreCase(schoolViewModel.getSelectedSchoolId())) {
                currentSchool = s;
            }
        }
        schoolOverview.setText(currentSchool.getOverviewParagraph());
        schoolName.setText(currentSchool.getSchoolName());
        String state = currentSchool.getStateCode();
        String city = currentSchool.getCity();
        String zip = currentSchool.getZip();
        String streetAddress = currentSchool.getPrimaryAddressLine1();
        address2.setText(String.format(res.getString(R.string.address_town_state_zip), city, state, zip));
        address1.setText(streetAddress);
        if (schoolScore != null && schoolScore.containsKey(schoolId)) {
            resultCardHolder.setVisibility(View.VISIBLE);
            noDataMsg.setVisibility(View.GONE);
            try {
                String satTaker = schoolScore.get(schoolId).getNumOfSatTestTakers();
                String mathScore = schoolScore.get(schoolId).getSatMathAvgScore();
                String readingScore = schoolScore.get(schoolId).getSatCriticalReadingAvgScore();
                String writingScore = schoolScore.get(schoolId).getSatWritingAvgScore();
                satTakers.setText(String.format(res.getString(R.string.no_of_sat_takers), satTaker));
                scoreWriting.setText(String.format(res.getString(R.string.score_writing), writingScore));
                scoreMath.setText(String.format(res.getString(R.string.score_math), mathScore));
                scoreCriticalReading.setText(String.format(res.getString(R.string.score_critical), readingScore));

            } catch (Exception e) {
                Log.e(TAG, "populateSchoolDetailUI: ", e);

            }

        } else {
            resultCardHolder.setVisibility(View.GONE);
        }

        if (currentSchool != null) {
            noDataMsg.setText(getContext().getResources().getString(R.string.result_not_available_txt));
        }

    }

    private void getScore() {
        NYCSchoolsNetworkCommunicator nycSchoolsNetworkCommunicator = new NYCSchoolsNetworkCommunicator();
        nycSchoolsNetworkCommunicator.getScore( new Subscriber<List<SchoolScore>>() {

            @Override
            public void onSubscribe(Subscription s) {

            }

            @Override
            public void onNext(List<SchoolScore> responseScore) {
                Map<String, SchoolScore> schoolScore = new HashMap<>();
                for (SchoolScore num : responseScore)
                {
                    schoolScore.put(num.getDbn(), num);
                }
                schoolViewModel.setSchoolScoreMap(schoolScore);
            }

            @Override
            public void onError(Throwable t) {
                setVisibilityForNoDataView();
                networkErrorTxt.setText(getContext().getResources().getString(R.string.network_error));

            }

            @Override
            public void onComplete() {

            }
        });
    }

    //This methods opens google map and starts navigation
    private void navigateToGoogleMaps() {
        try{

            String navigationString = "google.navigation:q=an+" +currentSchool.getPrimaryAddressLine1() +"+" + currentSchool.getCity();
            Intent mapIntent = new Intent(android.content.Intent.ACTION_VIEW,
                    Uri.parse(navigationString));
            if (mapIntent.resolveActivity(getContext().getPackageManager()) != null) {
                getContext().startActivity(mapIntent);
            }
        } catch(Exception e) {
            Toast.makeText(getContext(), "Can not perform the task: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //This methods helps to call from the device
    private void callToSchool() {
        try{
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            String phoneNumber ="tel:" + currentSchool.getPhoneNumber().trim();
            Intent callIntent = new Intent(Intent.ACTION_CALL,Uri.parse(phoneNumber));

            getContext().startActivity(callIntent);
        }catch (Exception e) {
            Toast.makeText(getContext(), "Can not perform the task: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

}