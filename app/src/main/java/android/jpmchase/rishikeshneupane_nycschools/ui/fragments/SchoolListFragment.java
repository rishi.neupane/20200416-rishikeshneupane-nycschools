package android.jpmchase.rishikeshneupane_nycschools.ui.fragments;

import android.jpmchase.rishikeshneupane_nycschools.data.model.School;
import android.jpmchase.rishikeshneupane_nycschools.data.net.NYCSchoolsNetworkCommunicator;
import android.jpmchase.rishikeshneupane_nycschools.ui.adapter.SchoolListAdapter;
import android.jpmchase.rishikeshneupane_nycschools.ui.main.MainActivity;
import android.jpmchase.rishikeshneupane_nycschools.utilties.CollectionUtiliy;
import android.jpmchase.rishikeshneupane_nycschools.viewModel.SchoolViewModel;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.jpmchase.rishikeshneupane_nycschools.R;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.List;

/**
 * This fragments holds lists of school.
 */
public class SchoolListFragment extends Fragment {

    private SchoolViewModel schoolViewModel;
    private TextView noDataMsg;
    private TextView networkErrorTxt;
    private Button retryBtn;
    private EditText searchEditText;
    private ConstraintLayout networkErrorView;
    private RecyclerView schoolListRecyclerView;
    private SchoolListAdapter schoolListAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<School> schoolDetails;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_school_list, container, false);
        schoolListRecyclerView = root.findViewById(R.id.school_list_rv);
        noDataMsg = root.findViewById(R.id.loading_data_msg);
        searchEditText = root.findViewById(R.id.search);
        retryBtn = root.findViewById(R.id.retry_btn);
        networkErrorTxt = root.findViewById(R.id.error_message_txt);
        networkErrorView = root.findViewById(R.id.network_error_view);
        schoolListRecyclerView.setHasFixedSize(true);
        schoolViewModel = new ViewModelProvider(getActivity()).get(SchoolViewModel.class);

        if(getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).setToolbarTitle(getString(R.string.nyc_shools));
            ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }

        if (schoolViewModel.getSchoolList().getValue() != null) {
            populateSchoolList(schoolViewModel.getSchoolList().getValue());
        } else {
            //Checking network connection before calling api
            if (getActivity() instanceof MainActivity && ((MainActivity) getActivity()).isNetworkAvailable()) {
                setLoadingMessageVisibility();
                networkErrorView.setVisibility(View.GONE);
                getSchools();
            } else {
                setVisibilityForNoDataView();
                networkErrorTxt.setText(getContext().getResources().getString(R.string.no_network_message));
                //Toast.makeText(getActivity(), R.string.no_network_message, Toast.LENGTH_SHORT).show();
            }
        }

        schoolViewModel.getSchoolList().observe(getViewLifecycleOwner(), new Observer<List<School>>() {

            @Override
            public void onChanged(List<School> school) {

                populateSchoolList(school);
                schoolDetails = school;
            }
        });

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                schoolDetails = CollectionUtiliy.filterSchools(schoolViewModel.getSchoolList().getValue(), editable.toString());
                schoolListAdapter.filterList(schoolDetails);
            }
        });

        retryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() instanceof MainActivity && ((MainActivity) getActivity()).isNetworkAvailable()) {
                    setLoadingMessageVisibility();
                    networkErrorView.setVisibility(View.GONE);
                    getSchools();
                }

            }
        });
        return root;
    }

    private void populateSchoolList(List<School> schools) {
        setLoadingMessageVisibility();
        layoutManager = new LinearLayoutManager(getContext());
        schoolListRecyclerView.setLayoutManager(layoutManager);
        schoolListAdapter = new SchoolListAdapter(this, schools);
        schoolListRecyclerView.setAdapter(schoolListAdapter);
    }

    private void setLoadingMessageVisibility() {
        if (schoolViewModel.getSchoolList().getValue() != null) {
            noDataMsg.setVisibility(View.GONE);
            searchEditText.setVisibility(View.VISIBLE);
        } else {
            noDataMsg.setVisibility(View.VISIBLE);
            searchEditText.setVisibility(View.GONE);
        }
    }

    private void setVisibilityForNoDataView() {
        noDataMsg.setVisibility(View.GONE);
        searchEditText.setVisibility(View.INVISIBLE);
        networkErrorView.setVisibility(View.VISIBLE);
    }

    private void getSchools() {
        //TODO nice to show spinner while requesting network
        NYCSchoolsNetworkCommunicator nycSchoolsNetworkCommunicator = new NYCSchoolsNetworkCommunicator();
        nycSchoolsNetworkCommunicator.getSchools( new Subscriber<List<School>>() {

            @Override
            public void onSubscribe(Subscription s) {

            }

            @Override
            public void onNext(List<School> schools) {
                schoolViewModel.setSchoolList(schools);
            }

            @Override
            public void onError(Throwable t) {
                setVisibilityForNoDataView();
                networkErrorTxt.setText(getContext().getResources().getString(R.string.network_error));

            }

            @Override
            public void onComplete() {

            }
        });
    }

    public void setSelectedSchoolData(int position) {
        //saves current index and id in viewModel
        schoolViewModel.setSelectedSchoolId(schoolDetails.get(position).getDbn());
        searchEditText.setText("");

        //switches the fragment
        SchoolResultsFragment schoolResultsFragment = new SchoolResultsFragment();
        FragmentTransaction transaction = getParentFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, schoolResultsFragment);
        transaction
                .addToBackStack("detail_view")
                .commit();

    }

}