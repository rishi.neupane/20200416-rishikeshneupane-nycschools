package android.jpmchase.rishikeshneupane_nycschools.ui.adapter;

import android.jpmchase.rishikeshneupane_nycschools.R;
import android.jpmchase.rishikeshneupane_nycschools.data.model.School;
import android.jpmchase.rishikeshneupane_nycschools.ui.fragments.SchoolListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class SchoolListAdapter extends RecyclerView.Adapter<SchoolListAdapter.MyViewHolder>{
    private List<School> school;
    private SchoolListFragment schoolListFragment;

    static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView schoolName;
        private TextView schoolAddress;
        private Button detailButton;


        private MyViewHolder(View v) {
            super(v);
            schoolName = v.findViewById(R.id.school_name);
            schoolAddress = v.findViewById(R.id.school_address);
            detailButton = v.findViewById(R.id.detail_btn);

        }
    }

    public SchoolListAdapter(SchoolListFragment schoolListFragment, List<School> school) {
        this.schoolListFragment = schoolListFragment;
        this.school = school;
    }

    @Override
    public SchoolListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.school_list, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.schoolName.setText(school.get(position).getSchoolName());
        holder.schoolAddress.setText(school.get(position).getPrimaryAddressLine1() + ", "
                + school.get(position).getCity());
        holder.detailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                schoolListFragment.setSelectedSchoolData(position);
            }
        });
    }


    @Override
    public int getItemCount() {
        if(school != null) {
            return school.size();
        }
        return 0;
    }

    public void filterList(List<School> school) {
        this.school = school;
        notifyDataSetChanged();
    }

}
