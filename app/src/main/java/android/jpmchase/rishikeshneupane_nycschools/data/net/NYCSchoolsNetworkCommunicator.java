package android.jpmchase.rishikeshneupane_nycschools.data.net;

import android.jpmchase.rishikeshneupane_nycschools.data.model.SchoolScore;
import android.jpmchase.rishikeshneupane_nycschools.data.model.School;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.Callback;
import org.reactivestreams.Subscriber;

import java.util.List;


public class NYCSchoolsNetworkCommunicator {
    String trackTeamBaseUrl = "https://data.cityofnewyork.us/";

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(trackTeamBaseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build();


    //This methods calls for NYC Schools
    public void getSchools(final Subscriber<List<School>> subscriber) {

        NYCSchoolsApiServicesInterface nycSchoolsApiServicesInterface = retrofit.create(NYCSchoolsApiServicesInterface.class);
        Call<List<School>> schools = nycSchoolsApiServicesInterface.getSchools();
        schools.enqueue(new Callback<List<School>>() {

            @Override
            public void onResponse(Call<List<School>> call, Response<List<School>> response) {
                subscriber.onNext(response.body());

            }

            @Override
            public void onFailure(Call<List<School>> call, Throwable t) {

            }
        });
    }

    //This methods calls for Scores using retrofit
    public void getScore(final Subscriber<List<SchoolScore>> subscriber) {

        NYCSchoolsApiServicesInterface nycSchoolsApiServicesInterface = retrofit.create(NYCSchoolsApiServicesInterface.class);
        Call<List<SchoolScore>> schools = nycSchoolsApiServicesInterface.getScores();
        schools.enqueue(new Callback<List<SchoolScore>>() {

            @Override
            public void onResponse(Call<List<SchoolScore>> call, Response<List<SchoolScore>> response) {
                subscriber.onNext(response.body());

            }

            @Override
            public void onFailure(Call<List<SchoolScore>> call, Throwable t) {

            }
        });
    }


}
