package android.jpmchase.rishikeshneupane_nycschools.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SchoolScore {

    @SerializedName("dbn")
    @Expose
    private var dbn: String? = null

    @SerializedName("school_name")
    @Expose
    private var schoolName: String? = null

    @SerializedName("num_of_sat_test_takers")
    @Expose
    private var numOfSatTestTakers: String? = null

    @SerializedName("sat_critical_reading_avg_score")
    @Expose
    private var satCriticalReadingAvgScore: String? = null

    @SerializedName("sat_math_avg_score")
    @Expose
    private var satMathAvgScore: String? = null

    @SerializedName("sat_writing_avg_score")
    @Expose
    private var satWritingAvgScore: String? = null

    fun getDbn(): String? {
        return dbn
    }

    fun setDbn(dbn: String?) {
        this.dbn = dbn
    }

    fun getSchoolName(): String? {
        return schoolName
    }

    fun setSchoolName(schoolName: String?) {
        this.schoolName = schoolName
    }

    fun getNumOfSatTestTakers(): String? {
        return numOfSatTestTakers
    }

    fun setNumOfSatTestTakers(numOfSatTestTakers: String?) {
        this.numOfSatTestTakers = numOfSatTestTakers
    }

    fun getSatCriticalReadingAvgScore(): String? {
        return satCriticalReadingAvgScore
    }

    fun setSatCriticalReadingAvgScore(satCriticalReadingAvgScore: String?) {
        this.satCriticalReadingAvgScore = satCriticalReadingAvgScore
    }

    fun getSatMathAvgScore(): String? {
        return satMathAvgScore
    }

    fun setSatMathAvgScore(satMathAvgScore: String?) {
        this.satMathAvgScore = satMathAvgScore
    }

    fun getSatWritingAvgScore(): String? {
        return satWritingAvgScore
    }

    fun setSatWritingAvgScore(satWritingAvgScore: String?) {
        this.satWritingAvgScore = satWritingAvgScore
    }


}