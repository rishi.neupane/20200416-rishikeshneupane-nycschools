package android.jpmchase.rishikeshneupane_nycschools.data.net;

import android.jpmchase.rishikeshneupane_nycschools.data.model.SchoolScore;
import android.jpmchase.rishikeshneupane_nycschools.data.model.School;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface NYCSchoolsApiServicesInterface {

    @GET("resource/s3k6-pzi2.json")
    Call<List<School>> getSchools();

    @GET("resource/f9bf-2cp4.json")
    Call<List<SchoolScore>> getScores();
}
