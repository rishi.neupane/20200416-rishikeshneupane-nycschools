package android.jpmchase.rishikeshneupane_nycschools.utilties;

import android.jpmchase.rishikeshneupane_nycschools.data.model.School;

import java.util.ArrayList;
import java.util.List;

public class CollectionUtiliy {
    public static List<School> filterSchools(List<School> schools, String text) {
        List<School> filteredNames = new ArrayList<>();
        for (School s : schools) {
            if (s.getSchoolName().toLowerCase().contains(text.trim().toLowerCase())) {
                filteredNames.add(s);
            }
        }
        return  filteredNames;
    }
}
