package android.jpmchase.rishikeshneupane_nycschools.viewModel;

import android.jpmchase.rishikeshneupane_nycschools.data.model.SchoolScore;
import android.jpmchase.rishikeshneupane_nycschools.data.model.School;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;
import java.util.Map;

public class SchoolViewModel extends ViewModel {
    private final MutableLiveData<List<School>> schoolList = new MutableLiveData<>();
    private final MutableLiveData<Map<String, SchoolScore>> schoolScoreMap = new MutableLiveData<>();
    private final MutableLiveData<String> noDataTextMessage = new MutableLiveData<>();
    private String selectedSchoolId = null;

    public SchoolViewModel() {

    }

    public void setNoDataTextMessage(String message) {
        noDataTextMessage.setValue(message);
    }

    public MutableLiveData<String> getNoDataTextMessage() {
        return noDataTextMessage;
    }

    public String getSelectedSchoolId() {
        return selectedSchoolId;
    }

    public void setSelectedSchoolId(String schoolId) {
        selectedSchoolId = schoolId;
    }

    public MutableLiveData<List<School>> getSchoolList() {
        return schoolList;
    }

    public MutableLiveData<Map<String, SchoolScore>> getSchoolScoreMap() {
        return schoolScoreMap;
    }

    public void setSchoolScoreMap(Map<String, SchoolScore> schoolScore) {
        schoolScoreMap.setValue(schoolScore);
    }

    public void setSchoolList(List<School> schoolListResponse) {
        schoolList.setValue(schoolListResponse);
    }

}
